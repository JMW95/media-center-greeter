// Which user to login as
const USER = "media";
const PASS = "media";
var session_to_start = null;

// Track the selection location
var sessions_buttons = [];
var footer_buttons = [];
var nav_position_in_footer = false;
var nav_position_buttons = 0;
var nav_position_footer = 0;

// Direction keys map
const dir_keys_map = {
    // Arrow keys
    37: "left",
    38: "up",
    39: "right",
    40: "down",
    // WASD
    65: "left",
    87: "up",
    68: "right",
    83: "down",
};

// Session name map
var session_names = [];

var rename_map = {
    "xfce": "Desktop",
    "steamos": "Steam",
};

function init() {
    if ("undefined" === typeof lightdm) {
        window.lightdm = {
            sessions: [
            {name: "Chromium", key: "chromium"},
            {name: "Kodi", key: "kodi"},
            {name: "Steam OS", key: "steamos"},
            {name: "Xfce Session", key: "xfce"},
            ],
        };
    }

    for (var i = 0; i < lightdm.sessions.length; i++) {
        var session = lightdm.sessions[i];
        var b = $("<button></button>");
        var s = $("<span></span>");
        var name = rename_map[session.key] || session.name;
        s.text(name);
        s.appendTo(b);
        b.css("background-image", "url(assets/icons/" + session.key + ".png)");
        b.focus(function() {
            var $sessions = $("#sessions");

            // Get my position relative to $sessions.
            // This is affected by scale, so we need to correct for that.
            var pos = $(this).position();
            var trueWidth = $(this).outerWidth(false);
            var scaledWidth = this.getBoundingClientRect().width;
            var scaleAdjust = (scaledWidth - trueWidth) / 2;
            var leftPos = pos.left + scaleAdjust;

            // Align my center with the screen center
            var widthWithMargins = $(this).outerWidth(true);
            var screenCenterX = $(window).width() / 2;
            var newLeft = screenCenterX - leftPos - (widthWithMargins / 2);
            $sessions.css("left", newLeft);

            // Track self as focussed
            nav_position_in_footer = false;
            nav_position_buttons = sessions_buttons.indexOf(this);
        });
        b.click(function() {
            var idx = sessions_buttons.indexOf(this);
            login_and_start_session(session_names[idx]);
        });
        b.appendTo($("#sessions"));
        sessions_buttons.push(b[0]);
        session_names.push(session.key);
    }

    // Start with the second thing focussed
    if (sessions_buttons.length > 1) {
        nav_position_buttons = 1;
    }
    focusSelected();

    $("#shutdown").click(function(event) {
        lightdm.shutdown();
    });

    $("#restart").click(function(event) {
        lightdm.restart();
    });

    $("#footer button").each(function(idx, elem) {
        footer_buttons.push(elem);
        $(elem).focus(function() {
            // Track self as focussed
            nav_position_in_footer = true;
            nav_position_footer = footer_buttons.indexOf(this);
        });
    });

    lightdm.show_prompt.connect((text, type) => {
        if (type == 1) {
            lightdm.respond(PASS);
        } else {
            $("#messages").text("prompt: " + type + ": " + text);
        }
    });

    lightdm.authentication_complete.connect(() => {
        if (lightdm.is_authenticated) {
            show_message("Starting: " + session_to_start, "success");
            lightdm.start_session(session_to_start);
        } else {
            show_message("Login failed.", "error");
        }
    });
}

function focusSelected() {
    for (var i = 0; i < footer_buttons.length; i++) {
        if ((i == nav_position_footer) && nav_position_in_footer) {
            $(footer_buttons[i]).focus();
        }
    }
    for (var i = 0; i < sessions_buttons.length; i++) {
        if ((i == nav_position_buttons) && !nav_position_in_footer) {
            $(sessions_buttons[i]).focus();
        }
    }
}

function move(dir) {
    if (nav_position_in_footer) {
        switch (dir) {
        case "up":
            nav_position_in_footer = false;
            break;
        case "left":
            nav_position_footer = Math.max(0, nav_position_footer-1);
            break;
        case "right":
            nav_position_footer = Math.min(footer_buttons.length-1, nav_position_footer+1);
            break;
        }
    } else {
        switch (dir) {
        case "down":
            nav_position_in_footer = true;
            break;
        case "left":
            nav_position_buttons = Math.max(0, nav_position_buttons-1);
            break;
        case "right":
            nav_position_buttons = Math.min(sessions_buttons.length-1, nav_position_buttons+1);
            break;
        }
    }

    focusSelected();
}

function show_message(text, type) {
    var $messages = $("#messages");
    $messages.text(text);
    if (type === "error") {
        $messages.css("color", "red");
    } else {
        $messages.css("color", "white");
    }
}

function login_and_start_session(key) {
    if (lightdm.in_authentication) return;

    show_message("Logging in...", "info");

    session_to_start = key;
    lightdm.authenticate(USER);
}

$(document).ready(function() {
    init();
});

$(document).on("mousedown", function(e) {
    if (e.target.nodeName != "BUTTON") {
        // Prevent blurring buttons
        e.preventDefault();
    }
});

$(document).on("keydown", function(e) {
    var dir = dir_keys_map[e.keyCode];
    if (dir) {
        move(dir);
    }
});
